use std::str::FromStr;

pub fn gen_directory(proto_base_dir: &str, output_module_path: &str) -> Result<Vec<String>, std::io::Error> {
	println!("cargo:rerun-if-changed={}", &proto_base_dir);

	// NOTE (JB) Get all protobuf files
	let all_protobuf = std::fs::read_dir(&proto_base_dir)
		.unwrap()
		.filter_map(|entry| {
			let path = match entry {
				Ok(entry) => entry.path().to_str().unwrap().to_string(),
				_ => return None,
			};

			if std::path::PathBuf::from_str(&path).unwrap().extension().unwrap().to_str().unwrap() != "proto" {
				return None;
			}

			println!("cargo:rerun-if-changed={}", path);

			Some(path)
		})
		.collect::<Vec<_>>();

	// NOTE (JB) Create mod.rs for protobuf files
	let out_dir = std::path::PathBuf::from_str(output_module_path).unwrap();

	if out_dir.exists() {
		std::fs::remove_dir_all(&out_dir).unwrap()
	}

	if !all_protobuf.is_empty() {
		std::fs::create_dir_all(&out_dir).unwrap();

		let mod_str = all_protobuf
			.iter()
			.map(|proto| {
				format!(
					"pub mod {};\n",
					std::path::PathBuf::from_str(proto).unwrap().file_stem().unwrap().to_str().unwrap()
				)
			})
			.collect::<Vec<_>>()
			.join("");

		std::fs::write(out_dir.join("mod.rs"), mod_str).unwrap();

		let res = protoc_rust::Codegen::new()
			.include(&proto_base_dir)
			.inputs(&all_protobuf)
			.out_dir(&out_dir)
			.run();

		if let Err(e) = res {
			return Err(e);
		}

		let rust_files = std::fs::read_dir(&out_dir)
			.unwrap()
			.filter_map(|entry| {
				let path = match entry {
					Ok(entry) => entry.path().to_str().unwrap().to_string(),
					_ => return None,
				};

				if std::path::PathBuf::from_str(&path).unwrap().extension().unwrap().to_str().unwrap() != "rs" {
					return None;
				}

				Some(path)
			})
			.collect::<Vec<_>>();

		return Ok(rust_files)
	}
	else {
		return Ok(Vec::new());
	}
}
